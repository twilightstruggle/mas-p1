# Auth with React Router V4 and Firebase V3
This is a webportal for authenticating with Firebase and React Router.

*Using React 15.4.0, React Router 4, and Firebase 3.6.1*

#### Features:
* Protected Routes with React Router
* Login/Logout Functionality for google login
* Simple Boostrap UI
* Npm Date-picker
* Axios for api call

#### Instructions:
* firebase config in ```config/constants```
* ```npm install```
* ```npm start```
* Visit ```localhost:3000```

#### To build for deploy
* ```npm run build```

#### To deploy to firebase 
* ```deploy firebase```
