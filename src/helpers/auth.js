//all authorization methods are stored here
import {firebaseAuth, provider } from '../config/constants'


export function logout () {
  localStorage.removeItem('eId');
  return firebaseAuth().signOut();   
}

export function loginGoogle () {
    provider.setCustomParameters({
    prompt: 'select_account'
  });
  return firebaseAuth().signInWithPopup(provider);
}