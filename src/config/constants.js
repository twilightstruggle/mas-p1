//file contains all the firebase credentuals for login and logout
import firebase from 'firebase'

const config = {
    apiKey: "AIzaSyBM4tUtZEdvj6OWix2eAEsFnPZBNfBH5Bk",
    authDomain: "ticketr-c494e.firebaseapp.com",
    databaseURL: "https://ticketr-c494e.firebaseio.com",
    projectId: "ticketr-c494e",
    storageBucket: "ticketr-c494e.appspot.com",
    messagingSenderId: "532937628544"
}

firebase.initializeApp(config)
export const provider = new firebase.auth.GoogleAuthProvider();
export const firebaseAuth = firebase.auth
