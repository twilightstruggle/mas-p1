import React, { Component } from 'react';
import { loginGoogle } from '../helpers/auth';
import axios from "axios";

import _ from 'lodash';
import SearchBar from './components/search_bar'
import YTSearch from 'youtube-api-search';
import VideoList from './components/video_list';
import VideoDetail from './components/video_detail'

const API_KEY = 'AIzaSyCcfhbP8Ga8Vu5fBJsryRgOO50IYai-wok';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      loginMessage: null, 
      videos: [],
      selectedVideo:  null,
    }
    this.search('surfboards');
  }

  //resets error state back to null if user start typing again
  errorReset = () => {
    if(this.state.loginMessage) {
      this.setState({ loginMessage: null }); 
      }
  }
  /*when user click submit, api call gets event id and compares to typed in event to login, else displays an correct error message*/
  handleSubmit = (e) => {
    e.preventDefault()
    if(this.eid.value !== "") {
      axios.get(`https://ticketr-api.herokuapp.com/getEvent/?event=${this.eid.value}`)
      .then((isEId) => {
        if(isEId.data) {
          loginGoogle();         
        } else {
          this.setState({ loginMessage: "Invalid Event Id" });
        }
      });
    } else{
      this.setState({ loginMessage: "No Event Id Entered" });
    }    
    localStorage.setItem('eId', this.eid.value);
    this.eid.value = "";
  }

  search(term){
    YTSearch({key: API_KEY, term: term}, (videos) =>{
      this.setState({
        videos: videos,
        selectedVideo: videos[0]
      });
    });
  };

  render () {
    return (
      <div>
        <p>Time is precious. Don't waste it standing in line.</p>
        <div className="col-sm-6 col-sm-offset-0">
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <label>Event ID</label>
              <input className="form-control" onChange={ this.errorReset } ref={(eid) => this.eid = eid} placeholder="Enter an Event Id"/>
            </div>
            {
            this.state.loginMessage &&
            <div className="alert alert-danger" role="alert">
              <span className="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
              <span className="sr-only">Error:</span>
              &nbsp;{this.state.loginMessage}
            </div>
            } 
            <button type="submit" className="loginBtn loginBtn--google" >Submit and Login with Google</button>
          </form>
        </div>
        <SearchBar onSearchTermChange={this.search.bind(this)}/>
            <VideoDetail video={this.state.selectedVideo} />
            <VideoList
            onVideoSelect={selectedVideo => this.setState({selectedVideo})}
            videos={this.state.videos} />
      </div>
    )
  }
}
