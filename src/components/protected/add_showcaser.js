import React, { Component } from 'react'

export default class AddShowcaser extends Component {
  
  handleSubmit = (e) => {
    e.preventDefault()
    this.props.addShowcaserForm(this.showcaser.value, this.info.value, this.limit.value);
    this.showcaser.value = "";
    this.info.value = "";
    this.limit.value = "";
  }

  render () {
    return (
      <div className="col-sm-8 col-sm-offset-2">
        <h1>Add new Showcaser</h1>
        <form onSubmit={this.handleSubmit}>
          <div className="form-group">
            <label>Showcaser</label>
            <input className="form-control" ref={(showcaser) => this.showcaser = showcaser} placeholder="Showcaser Name"/>
          </div>
          <div className="form-group">
            <label>Info</label>
            <input className="form-control" placeholder="Info" ref={(info) => this.info = info} />
          </div>
          <div className="form-group">
            <label>Limit</label>
            <input className="form-control" placeholder="Queue Limit" ref={(limit) => this.limit = limit} />
          </div>
          <button type="submit" className="btn btn-success">Submit</button>
        </form>
      </div>
    )
  }
}
