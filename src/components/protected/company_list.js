import React from 'react';
import CompanyListItem from './company_list_item';

const CompanyList = ({ companies }) => {
  //uses state arrays passed in from dashboard to create an array of company_list_items components and list them
  const companyItems = companies.map((company) => {
      return (
        <CompanyListItem
          key={company.name}
          company={company}/>
      );
    });

  return (
    <div>
      <ul className="col-md-4 list-group">
        {companyItems}
      </ul>
    </div>
  );
}

export default CompanyList;
