import React, { Component } from 'react'
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';
import axios from 'axios';
import { firebaseAuth } from '../../config/constants';

export default class PickDate extends Component {

  constructor (props) {
    super(props)
    this.state = {
      startDate: moment()
    };
    this.handleChange = this.handleChange.bind(this);
  }

  //when date field is selected and new date is picked, an api call updates events date on data base
  handleChange(date) {
    this.setState({
      startDate: date
    });
    axios.get(`https://ticketr-api.herokuapp.com/eventDate/?org=${firebaseAuth().currentUser.uid}&event=${localStorage.getItem('eId')}&date=${date._d}`);    
  }

  render () {
    return (
      <div className="col-sm-0 col-sm-offset-8">
        <h1> Event Date </h1>
        <DatePicker selected={this.state.startDate} onChange={this.handleChange} />
      </div>
    );
  }
}
