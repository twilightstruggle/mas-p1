import React from 'react';
  //by receving a company from company_list component the name, info, and limit are added to each field 
const CompanyListItem = ({company}) => {
  return (
    <li className="list-group-item">
      <div className="company-list media">
          <div className="media-left">
            Showcaser: {company.name}
              <div className="media-body">
                <div className="media-heading">Info: {company.info}</div>
                <div className="media-heading">Limit: {company.limit}</div>
              </div>
          </div>
        </div>
    </li>
  );
}

export default CompanyListItem;
