import React, { Component } from 'react';
import CompanyList from './company_list';
import AddShowcaser from './add_showcaser';
import axios from 'axios';
import PickDate from './pick_date';
import { logout } from '../../helpers/auth';
import { firebaseAuth } from '../../config/constants'

const ROOT_URL = `https://ticketr-api.herokuapp.com/`;

export default class Dashboard extends Component {
  //takes inital events state can converts to an array of objects instead of and object of objects and sets that array to the dashboards starting state
  constructor(props){
    super(props);

    this.state = {
      companies: []
     };
     axios.get(`${ROOT_URL}showcasers/?event=${localStorage.getItem('eId')}`)
       .then(response => {
         const toArray = Object.keys(response.data).map((company) => {
             return {
               "name": company,
               info: response.data[company].info,
               limit: response.data[company].limit
             };
           });
         this.setState({
           companies: toArray
         })
       });
  }
  // checks to see if the person logging in is the events orginizer, if not then kicks then back to the login page
  componentWillMount() {
    axios.get(`https://ticketr-api.herokuapp.com/getEvent/?event=${localStorage.getItem('eId')}`)
      .then((isOrganizer) => {
        if(isOrganizer.data.organizer !== firebaseAuth().currentUser.uid) {
          logout();
        }
      });
  }

  //renders the date picker, add showcaser, and company list componets.
  render(){
    return (
      <div>
        <div className="pick-date">
              <PickDate />
        </div>

        <div className="company-detail col-md-8">
          <AddShowcaser addShowcaserForm={(showcaser, info, limit) => {
            const company = { name: showcaser, info: info, limit: limit  };
            axios.get(`${ROOT_URL}info/?showcase=${showcaser}&info=${info}&event=${localStorage.getItem('eId')}`);
            axios.get(`${ROOT_URL}limit/?showcase=${showcaser}&limit=${limit}&event=${localStorage.getItem('eId')}`);
            this.setState({
              companies: this.state.companies.concat(company)
            });
        } }/>
        </div>
        <CompanyList companies={this.state.companies} />
      </div>
    );
  }
}
